;;;; hybrid.asd

(asdf:defsystem #:hybrid
  :description "chat-forum hybrid"
  :author "araly"
  :license "license"
  :version "0.0.1"
  :serial t
  :depends-on nil
  :pathname "src"
  :components
  ((:file "package")
     (:file "main"))
  :build-operation "program-op"
  :build-pathname "hybrid"
  :entry-point "hybrid::main")
