;;;; src/main.lisp

(in-package :hybrid)

(defun main ()
  "main"
  (prog1 (format t "hybrid~%")
    (sb-ext:exit)))
